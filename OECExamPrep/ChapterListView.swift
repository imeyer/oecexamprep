//
//  ContentView.swift
//  OECExamPrep
//
//  Created by Ian Meyer on 8/30/19.
//  Copyright © 2019 Ian Meyer. All rights reserved.
//

import SwiftUI
import Combine

struct ChapterListView: View {
    var body: some View {
        NavigationView() {
            List() {
                ForEach(bookData.chapters, id: \.self) { chapter in
                    NavigationLink(destination: QuestionView(chapter: chapter)) {
                        ChapterRowView(chapter: chapter)
                    }
                }
                .padding(.top, 3)
            }
            .navigationBarTitle(Text("OEC Exam Prep"))
        }
    }
}

struct ChapterRowView: View {
    var chapter: Chapter
    var body: some View {
        HStack {
            Text("Chapter \(chapter.id) (\(chapter.questions.count) questions)")
                .padding(1)
                .font(.body)
        }
    }
}

struct ScoreCardCircle: View {
    var count = 0
    var color: Color
    var body: some View {
        ZStack(alignment: .center) {
            Circle().fill(color.opacity(0.60)).frame(width: 50, height: 50, alignment: .center)
            Text("\(count)")
                .bold()
                .padding(.horizontal)
                .font(.title)
                .foregroundColor(Color.white.opacity(0.90))
        }
        
    }
}

class ScoreData: ObservableObject {
    @Published var correct = 0
    @Published var incorrect = 0
}

struct QuestionView: View {
    @EnvironmentObject var scoreData: ScoreData
    var chapter: Chapter
    @State private var showAlert = false
    @State private var correctAnswerText = ""
    @State private var correctAnswerQuestion = ""

    var body: some View {
        VStack() {
            Text("Chapter \(chapter.id)")
                .font(.footnote)
                .fontWeight(.thin)
            HStack(alignment: .top) {
                ScoreCardCircle(count: scoreData.correct, color: Color.green)
                    .padding(.horizontal)
                Button(action: {
                    self.scoreData.correct = 0
                    self.scoreData.incorrect = 0
                }) {
                    Text("reset score")
                        .font(.subheadline)
                        .foregroundColor(Color.gray)
                }.padding()
                ScoreCardCircle(count: scoreData.incorrect, color: Color.red)
                    .padding(.horizontal)
            }
            ForEach([chapter.questions.shuffled().randomElement()!], id: \.self) { question in
                VStack(alignment: .leading) {
                    Text(question.body)
                        .font(.system(size: 22))
                        .padding()
                    Divider()
                    Button(action: {
                        self.checkAnswer("a", question)
                    }) {
                        Text(question.a)
                            .padding()
                    }

                    Divider()
                    Button(action: {
                        self.checkAnswer("b", question)
                    }) {
                        Text(question.b)
                            .padding()
                    }

                    Divider()
                    Button(action: {
                        self.checkAnswer("c", question)
                    }) {
                        Text(question.c)
                            .padding()
                    }

                    Divider()
                    Button(action: {
                        self.checkAnswer("d", question)
                    }) {
                        Text(question.d)
                            .padding()
                    }
                }
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
            }
        }.alert(isPresented: self.$showAlert, content: {
            Alert(title: Text(correctAnswerQuestion), message: Text("\(correctAnswerText)"), dismissButton: .default(Text("OK")))
        })
    }

    func checkAnswer(_ answer: String, _ question: Question) {

        if answer == question.correctAnswer {
            scoreData.correct += 1
            correctAnswerQuestion = ""
            correctAnswerText = ""
        } else {
            scoreData.incorrect += 1

            self.showAlert = true

            correctAnswerQuestion = question.body
            switch question.correctAnswer {
            case "a":
                correctAnswerText = question.a
            case "b":
                correctAnswerText = question.b
            case "c":
                correctAnswerText = question.c
            case "d":
                correctAnswerText = question.d
            default:
                correctAnswerText = "This is a bug."
            }
        }
    }
}

struct ChapterListView_Previews: PreviewProvider {
    static var previews: some View {
        ChapterListView()
    }
}
