//
//  Book.swift
//  OECExamPrep
//
//  Created by Ian Meyer on 8/30/19.
//  Copyright © 2019 Ian Meyer. All rights reserved.
//

import SwiftUI

struct Book: Hashable, Codable, Identifiable {
    var id: String
    var chapters: [Chapter]
}

struct Chapter: Hashable, Codable, Identifiable {
    var id: String
    var questions: [Question]
}

struct Question: Hashable, Codable, Identifiable {
    var id, body: String
    var a, b, c, d: String
    var correctAnswer: String
    var objective: String
    var reference: String
}

let bookData: Book = load("chapters.json")

func load<T: Decodable>(_ filename: String, as type: T.Type = T.self) -> T {
    let data: Data

    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
        else {
            fatalError("Couldn't find \(filename) in main bundle.")
    }

    do {
        data = try Data(contentsOf: file)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }

    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}
